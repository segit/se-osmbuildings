## TODO

### shadows 
  * render only shadows option
    - calc the difference between current shadow poly and building poly
    - may use http://turfjs.org/docs/#difference
  * calc shadow overlap with roof
    - get the overlapping poly: shape, area, in time...
    - may use http://turfjs.org/docs/#intersect

### roof shapes
  


## What could be next steps for OSM Buildings

- one more roof shape
- roof color by building type
- handle render modes as plugins
- new adapters, i.e. for ArcGIS, Google Maps, Here
